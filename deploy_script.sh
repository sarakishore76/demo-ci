#!/bin/bash
NEXUS_URL="http://192.168.1.30:8081/repository/demo-ci/com/logicfocus/demo-ci/1.0.1/demo-ci-1.0.1-plain.jar"
JAR_FILE="demo-ci-1.0.1-plain.jar"
DESTINATION="/home/logicfocus/Demo-ci"
USERNAME="admin"
PASSWORD="lfadmin"
# Ensure the destination directory exists
mkdir -p "${DESTINATION}"
# Download the JAR file using curl with environment variables for credentials
curl -u "${USERNAME}:${PASSWORD}" -o "${DESTINATION}/${JAR_FILE}" "${NEXUS_URL}" -v
# Check if the curl command was successful
if [ $? -eq 0 ]; then
    echo "Download completed successfully."
else
    echo "Download failed."
    exit 1
fi