FROM openjdk:11
ADD build/libs/gradleSampleArt-1.0.1.jar demo-ci.jar
EXPOSE 8097
ENTRYPOINT ["java", "-jar", "demo-ci.jar"]